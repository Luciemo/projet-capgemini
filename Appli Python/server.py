# coding: utf-8
import http.server
import socketserver

PORT = 8080
address = ("", PORT)


handler = http.server.SimpleHTTPRequestHandler 
httpd = socketserver.TCPServer(address, handler)

print ("Serveur démarré sur le port {}".format(PORT))

httpd.serve_forever()